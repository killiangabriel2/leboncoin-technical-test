package com.techtest.leboncoin.ui.album

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.techtest.leboncoin.R
import com.techtest.leboncoin.domain.Album

class AlbumAdapter(private val albumList: List<Album>):
    RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {
    class AlbumViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        private var album: Album? = null

        init {
            // Define click listener for the ViewHolder's View.
            textView = view.findViewById(R.id.album_item_fragment_title)
        }

        fun bind(album: Album) {
            this.album = album
            textView.text = "title : ${album.albumId} / ${album.songs.size} songs"
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): AlbumViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.album_item_fragment, viewGroup, false)

        return AlbumViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: AlbumViewHolder, position: Int) {
        val album = albumList[position]
        viewHolder.bind(album)
    }

    override fun getItemCount() = albumList.size
}