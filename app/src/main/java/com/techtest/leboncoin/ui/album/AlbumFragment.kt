package com.techtest.leboncoin.ui.album

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.techtest.leboncoin.R
import com.techtest.leboncoin.domain.Album
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AlbumFragment : Fragment() {

    companion object {
        fun newInstance() = AlbumFragment()
    }

    private val viewModel: AlbumViewModel by viewModels()
    private lateinit var albumAdapter: AlbumAdapter
    private lateinit var albumRecyclerView: RecyclerView
    private val albumList = ArrayList<Album>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.album_fragment, container, false)
        initVars(view)
        fetchAndObserveAlbums()
        return view
    }

    private fun initVars(view: View) {
        albumAdapter = AlbumAdapter(albumList)
        albumRecyclerView = view.findViewById(R.id.album_fragment_recycler_view)
        albumRecyclerView.adapter = albumAdapter
    }

    private fun fetchAndObserveAlbums() {
        viewModel.fetchAllAlbums()
        viewModel.albumsLiveData.observe(viewLifecycleOwner) {
            albumList.addAll(it)
            albumAdapter.notifyDataSetChanged()
        }
    }

}