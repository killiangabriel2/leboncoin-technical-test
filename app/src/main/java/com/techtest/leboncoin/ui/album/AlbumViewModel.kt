package com.techtest.leboncoin.ui.album

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.techtest.leboncoin.data.repository.ApiSongRepository
import com.techtest.leboncoin.domain.Album
import com.techtest.leboncoin.domain.Song
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Console
import javax.inject.Inject

@HiltViewModel
class AlbumViewModel @Inject constructor(
    private val apiSongRepository: ApiSongRepository
) : ViewModel() {

    val albumsLiveData = MutableLiveData<List<Album>>()

    fun fetchAllAlbums() {
        viewModelScope.launch {
            val response = apiSongRepository.fetchAllSongs()
                    when (response.isSuccessful) {
                        true -> {
                            val albumList = response.body()
                                ?.groupBy { it.albumId }
                                ?.map { (albumId, songs) ->
                                    Album(
                                        albumId = albumId,
                                        songs = songs
                                    )
                                } ?: emptyList()

                            albumsLiveData.postValue(albumList)
                        }
                        false -> {
                            Log.e(AlbumViewModel::class.simpleName,"fail fetch songs : ${response.message()}")
                        }
                    }
        }
    }

}