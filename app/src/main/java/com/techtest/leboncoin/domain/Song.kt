package com.techtest.leboncoin.domain

data class Song(
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)
