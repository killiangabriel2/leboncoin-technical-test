package com.techtest.leboncoin.domain

data class Album(
    val albumId: Int,
    val songs: List<Song>
)
