package com.techtest.leboncoin.data.api

import com.techtest.leboncoin.data.api.album.ApiSongService
import com.techtest.leboncoin.data.repository.ApiSongRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    private const val URL = "https://static.leboncoin.fr/"

    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Singleton
    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(URL)
        .client(okHttpClient)
        .build()

    @Singleton
    @Provides
    fun provideApiSongService(retrofit: Retrofit): ApiSongService = retrofit.create(
        ApiSongService::class.java)

    @Singleton
    @Provides
    fun provideApiSongRepository(apiSongService: ApiSongService): ApiSongRepository = ApiSongRepository(apiSongService)
}