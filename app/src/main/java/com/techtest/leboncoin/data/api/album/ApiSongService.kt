package com.techtest.leboncoin.data.api.album

import com.techtest.leboncoin.domain.Song
import retrofit2.Response
import retrofit2.http.GET

interface ApiSongService {

    @GET("img/shared/technical-test.json")
    suspend fun fetchAllSongs(): Response<List<Song>>
}