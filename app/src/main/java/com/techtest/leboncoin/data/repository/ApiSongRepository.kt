package com.techtest.leboncoin.data.repository

import com.techtest.leboncoin.data.api.album.ApiSongService
import com.techtest.leboncoin.domain.Song
import retrofit2.Response

class ApiSongRepository(private val apiSongService: ApiSongService) {
    suspend fun fetchAllSongs(): Response<List<Song>> {
        return apiSongService.fetchAllSongs()
    }
}